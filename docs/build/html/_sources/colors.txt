The Colors Module
***************
This really isn't much of a module, it is just a series of color constants.

Colors from dark_red onwards are courtesy of: http://www.w3.org/TR/css3-color/

List of color constants::

	red=(255, 0, 0)
	green=(0, 255, 0)
	blue=(0, 0, 255)
	black=(0, 0, 0)
	white=(255, 255, 255)
	dark_red=(139, 0, 0)
	dark_green=(0, 100, 0)
	dark_blue=(0, 0, 139)
	dark_gray=(169, 169, 169)
	gray=(128, 128, 128)
	light_gray=(211, 211, 211)
	yellow=(255, 255, 0)
	brown=(165, 42, 42)
	pink=(205, 133, 63)
	purple=(128, 0, 128)